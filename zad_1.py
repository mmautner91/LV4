# -*- coding: utf-8 -*-
"""
Created on Fri Dec 18 21:22:43 2015

@author: bec
"""

import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error
import math


def non_func(x):
    #definiranje izgleda izlaza,kombinacija trig,funk,nebitno
    y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
    return y
    
def add_noise(y):
    np.random.seed(14) 
            #napravi rendom brojeve ali uvijek neka seed(pocetni ) bude isti -14,valjda
                # valjda kada to napravi jednom vrijedi zauvijek da mu je seed konstantan
           
    varNoise = np.max(y) - np.min(y) #uzmi maximalne vrijd. izlaza, oduzmi od njih minimume
                                            #varnoise = 5.2108    
                            #vjerojatno uzima ovaj varNoise u def.y_noise čisto zato što moze, 
                           # i da se ipak izvede jedno iz drugoga, kao da bude povezano
    print 'varNoise',varNoise
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy

x = np.linspace(1,10,100)#od 1 do 10 100podjeljaka,jednakih, ovo je samo os
y_true = non_func(x)#pozvanje funkcije
#print 'x je jedanako ovome : \r\n  ', x
#print y_true
#plot(y_true)

y_measured = add_noise(y_true) #simulacija mjerenih rezultata

plt.figure(1)
plt.plot(x,y_measured,'ok',label='mjereno')
plt.plot(x,y_true,label='stvarno')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)

#rendom dodjeljivanje indexsa ulaznim i izlaznim rendompdatcima
np.random.seed(12)
indeksi = np.random.permutation(len(x))
indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))] #floor-zaokruživanje na manji cijeli
indeksi_test = indeksi[int(np.floor(0.7*len(x)))+1:len(x)]

x = x[:, np.newaxis] #-axis of lenght one-formatira x da bude u jednom stupcu -kul
#print 'x[:, np.newaxis]print',x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis] #formatira y da bude u jednom stupcu

xtrain = x[indeksi_train]
ytrain = y_measured[indeksi_train]
xtest = x[indeksi_test]
ytest = y_measured[indeksi_test]

#plot(xtrain)
#plot(xtest)


plt.figure(2)
plt.plot(xtrain,ytrain,'ob',label='train')
plt.plot(xtest,ytest,'or',label='test')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)

linearModel = lm.LinearRegression()
linearModel.fit(xtrain,ytrain)

print 'Model je oblika y_hat = Theta0 + Theta1 * x'
print 'y_hat = ', linearModel.intercept_, '+', linearModel.coef_, '*x'

ytest_p = linearModel.predict(xtest)
MSE_test = mean_squared_error(ytest, ytest_p)

plt.figure(3)
plt.plot(xtest,ytest_p,'og',label='predicted')
plt.plot(xtest,ytest,'or',label='test')
plt.legend(loc = 4)

x_pravac = np.array([1,10])
x_pravac = x_pravac[:, np.newaxis]
y_pravac = linearModel.predict(x_pravac)
plt.plot(x_pravac, y_pravac)

'''
Na podacima iz zadatka 1 potrebno je odrediti parametre linearnog modela na 
podacima za učenje na način da se implementira funkcija za izracunavanje 
parametara linearnog modela prema (4-10). Usporedite dobivene parametre
modela s vrijednostima parametara linearnog modela iz zadatka 1.
'''

x_kor=np.c_[np.ones(len(xtrain)),xtrain]
Thml=np.dot((np.dot((np.linalg.inv(np.dot(x_kor.transpose(),x_kor))),x_kor.transpose())),ytrain)
#print 'Thml-theta zatvorena forma je::' ,Thml

#thml-daje iste rezultate kao u zadatku 1



def costfun(x_kor, ytrain, Th):
    n = x_kor.shape[0]
    cost = (1./(2*n))*(x_kor*Th-ytrain).T*(x_kor*Th-ytrain) #kriterijska funkcija
        
    return cost.flat[0]

#print 'x_kor*Th:    ',x_kor*Th-ytrain
#print 'cost jeeeeeeeee',cost
#
#print 'cost flat',cost.flat[0]

#print 'theta = ep.matrix(',Th


def gradient(x_kor, ytrain, Th, iter, alpha):
    Th_iter = [] #record theta for each iteration
    cost_iter = []  #record cost for each iteration-> treba za izracunavanje kriterijske funk. J
    n = x_kor.shape[0] #uzima broj redaka za x_kor

    for i in range(iter):
        #update theta-dobiveno kombnaciom formula za gradijent ()
        Th = Th-(alpha/n)*x_kor.T*(x_kor*Th-ytrain)
#        print 'theta jeeeeeeeee',Th

#        print '\n',Th.T
#        print x_kor
     
        Th_iter[len(Th_iter):]=[Th]

        cost_iter[len(cost_iter):]=[costfun(x_kor,ytrain,Th)]
#        print 'cost iter u petlji',cost_iter
##        print Th_iter
    return(Th, Th_iter, cost_iter)

Th = np.matrix([[0.],[0.]])
alpha = 0.01 #mora biti manje od 0.05 inace nista ne valja, Th poprima jako velike 
                    #vrijednosti,ne manje od 0.01, opet ne valja nista

iter = 15
Th, Th_iter, cost_iter = gradient(x_kor, ytrain, Th, iter, alpha)
#print'Thhhhh jwwww \n', Th
#print 'Th_iter',Th_iter

ar_min=np.argmin(cost_iter)
print 'ar_minnnnnnnnnnnnnn ',cost_iter[ar_min] #vraca index u listi cost_ite gdje je 
                                    #cost_ite ima najmanju vrijdnosti
                                     #moram naci Th sa kojim se izracunava vrijdnost na tom indexu
                #indexi krecu od nula u cost_iter

print'cost_iter jeweeeeeee', cost_iter
Th_min=Th_iter [ar_min]
print 'Th_min jeeeeeeeeee',Th_min
#result = x_kor*Th
#x1=len(result)
#print 'Th_iter jeeeeeeeeeee \n',Th_iter

'''
ZADATAK 4
'''
n = x_kor.shape[0]
#x_kor=np.c_[np.ones(len(xtrain)),xtrain]

x_kor2=np.c_[np.ones(len(xtest)),xtest]
nn=x_kor2.shape[0]

mse_u=(1./(2*n))*(ytrain-x_kor*Th).T*(ytrain-x_kor*Th)
mse_t=(1./(2*nn))*((ytest-x_kor2*Th).T*(ytest-x_kor2*Th))


print'mse_u', mse_u
print'mse_t', mse_t

y_prorac_1=np.ones(len(ytrain))
print 'y_prorac_1',y_prorac_1
#ne radi formula, problemi sa formatiranjem djelova oko oduzimanja,ili dijeljenja 
#probao ./(ytrain-1), probao umjesto 1 y_prorac_1, probao *(1./(ytrain-1 (i -y_prorac_1))) ??????????
#R_u=1-(np.dot(((ytrain-x_kor*Th).T*(ytrain-x_kor*Th)),(1./(ytrain-y_prorac_1))))
'''
#potrebno dodatno izcuci korijen iz R ova
'''
R_u=1-((ytrain-x_kor*Th).T*(ytrain-x_kor*Th)) #dio koji radi

R_t=1-((ytest-x_kor2*Th).T*(ytest-x_kor2*Th))#1./(ytest-1) #ne radi iz nekog razloga-prijavljuje sintax

#math domain error prijavljuje
'''
r_u=math.sqrt(R_u)
r_t=math.sqrt(R_t)
'''
print 'R_uuuuu,dio koji radi', R_u

print'R_tttttttt,dio koji radi', R_t

result = x_kor*Th
#print'rezultat', result
plt.figure(4)
plt.plot(xtrain, result) #pravac
plt.scatter(xtrain, ytrain, marker='o', c='b')
plt.grid(True)
plt.xlabel('x')
plt.ylabel('y')
plt.xlim((0,11))
plt.ylim((-3,6))
plt.show()
plt.legend(loc = 4)
#
#print 'cost_iter',cost_iter
plt.figure(5)
#f=cost_iter.shape[1]
#print 'shape cost iter',f
# prikaz vrijednosti kriterijske funkcije 
x16=np.linspace(0,1,iter)
plt.plot(x16,cost_iter)
##plt.plot(np.arange(0, iter, 1), cost_iter)
##plt.xlabel('Iteration ')
##plt.ylabel('MSE- (eng.mean squared error)')
plt.show()
#
#linearModel.fit(xtrain,result)
print 'Model je oblika y_hat = Theta0 + Theta1 * x'
print 'dobivena vrijednost Theta0 + Theta1 *x Metodom gradijentnog spusta iznose',
print Th_min, 
print '\nvrijednosti dobivene primjenom zatvorene forme iznose',Thml
print '\n ar_min', ar_min  #zasto je namanji cost iter uvijek na zadnjem mjestu,xtest-sortirana lista ??



'''
ZADATAK 5
e (i) = y (i) − h θ (x (i) ) .
'''

ei=ytrain-x_kor*Th
#print 'eiii',ei

ei2=np.dot(ei.T,xtrain)
ei3=sort(ei2)
#print 'ei sort',ei3
plt.figure(6)

#plt.scatter(ei2,xtrain)
#plt.scatter(xtrain, ytrain, marker='o', c='b')
#plt.scatter(ei,xtrain,marker='o', c='b')
